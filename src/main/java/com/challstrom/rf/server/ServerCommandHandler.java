package com.challstrom.rf.server;

import com.challstrom.rf.core.RFCommandHandler;
import com.challstrom.rf.core.RFRoute;
import com.challstrom.rf.core.packet.RFPacket;

import java.net.DatagramPacket;
import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerCommandHandler extends RFCommandHandler {
    private final ConcurrentLinkedQueue<RFRoute> rfRoutes;

    public ServerCommandHandler(ConcurrentLinkedQueue<RFRoute> rfRoutes) {
        this.rfRoutes = rfRoutes;
    }

    @Override
    public void handleCommand(RFPacket rfPacket, DatagramPacket datagramPacket) {
        //TODO: Implement subscribe
        String command = new String(rfPacket.getEncodedMessage());
        System.out.println("COMMAND RECEIVED: " + command + " FROM " +datagramPacket.getAddress() + ":" + datagramPacket.getPort());
        if (Objects.equals(command, "!SUBSCRIBE")) {
            rfRoutes.add(new RFRoute(datagramPacket.getAddress(), datagramPacket.getPort()));
        }
        else if (Objects.equals(command, "!UNSUBSCRIBE")) {
            rfRoutes.remove(new RFRoute(datagramPacket.getAddress(), datagramPacket.getPort()));
        }
    }
}
