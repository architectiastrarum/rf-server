package com.challstrom.rf.server;

import com.challstrom.rf.core.RFCommandHandler;
import com.challstrom.rf.core.RFRoute;
import com.challstrom.rf.core.packet.RFPacketBuffer;
import com.challstrom.rf.core.packet.RFPacketReceiver;
import com.challstrom.rf.core.packet.RFPacketSender;
import com.challstrom.rf.core.packet.RFVerbose;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class Main {
    //TODO: read from config
    private static final ConcurrentLinkedQueue<RFRoute> rfRoutes = new ConcurrentLinkedQueue<>();
    static final AtomicBoolean systemIsRunning = new AtomicBoolean(true);

    public static void main(String[] args) {
        DatagramSocket packetSocket = null;
        try {
            packetSocket = new DatagramSocket(4157);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        RFVerbose.useVerbose = true;

        //setup  receiving
        RFPacketBuffer sharedBuffer = new RFPacketBuffer();
        RFCommandHandler commandHandler = new ServerCommandHandler(rfRoutes);
        RFPacketReceiver packetReceiver = new RFPacketReceiver(sharedBuffer, packetSocket, systemIsRunning, commandHandler);

        //setup sending
        RFPacketSender packetSender = new RFPacketSender(sharedBuffer, packetSocket, systemIsRunning, 500, rfRoutes);

        //setup thread pool, submit threads
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.submit(packetReceiver);
        executor.submit(packetSender);

        try {
            System.out.println("Server running on: " + InetAddress.getLocalHost() + ":" + (packetSocket != null ? packetSocket.getLocalPort() : 0));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}
